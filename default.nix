{
	nixpkgs ? import <nixpkgs> {},
	cache ? null,
	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nix-community/naersk/archive/master.tar.gz") {},
}:
with nixpkgs;
rec {
	statit = naersk.buildPackage {
		root = pkgs.nix-gitignore.gitignoreSource [".*" "*.nix"] ./.;
	};
}
