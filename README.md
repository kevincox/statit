# Easy Static Site Deployment

Currently only [Amazon S3](https://aws.amazon.com/s3/) is a supported target, however more targets may be added in the future.

## Features

- Fast parallel deploy.
- Delta sync avoids re-uploading existing files (saves time and cost).
- Awareness of [assets vs entries](https://kevincox.ca/2021/08/24/atomic-deploys/) ensures zero-downtime deploys.
- Smart defaults for `Content-Type` and `Cache-Control` with flexible overrides.
- Precise, smart cleanup of old files.

## Usage

### Basic

```sh
statit --bucket=my-bucket deploy www/ --dry-run
```

This will deploy the `www/` directory to `my-bucket` using the default settings. For example assets will be assumed to reside in `www/a/` and will be cached forever. Other files are considered entires and cached for 10min (with some error and invalidation leeway). Note that the default cache settings are subject to change at any time.

The `--dry-run` flag will evaluate what would have been done (including reading the bucket to calculate what the existing files are) but will not perform any writes. Remove this flag to actually perform the operation.

### Common Options

This section gives an overview of some common options. To see all options and reference documentation see `statit --help`. You can also see the [source definition in `args.rs`](src/args.rs).

#### Assets

By default, *assets* are assumed to reside in the `a/` subdirectory. You can customize this with the `--assets` flag. For example `--assets assets/**` if your *asset* directory is called `assets` or `--assets *.js --assets *.css` if you keep your *assets* at the top-level and identify them via extension.

statit categorizes each file as either an *asset* or an *entry*. The core difference is that *entries* have stable URLs. This means that anything that users will visit via link needs to be an *entry*. *Assets* are everything else. If you use a build tool it probably renames *assets* with a unique ID or hash in their name this means that each *asset* URL will always have the same content, allowing great caching.

The following rules are then applied:
- *Assets* are uploaded before *entries*.
	- This ensures that when a user loads a new *entry* its dependencies (like JS and CSS) are already available.
- *Assets* have a permanent cache setting (by default).
- After deploying, all unknown *entries* are removed.
	- This ensures that removed pages are removed from your site.
- *Assets* are garbage collected 7d after they were last used (on the next deploy).
	- This ensures that people using an old *entry* version (for example an old tab) can continue using your site without any issues but avoiding accumulating space forever.
	- This duration currently not configurable. (It would be easy to do, patches welcome.)

For more details about this system check out [You Don't Want Atomic Deploys](https://kevincox.ca/2021/08/24/atomic-deploys/).

#### Caching

- `--cache` sets the `Cache-Control` for matching files. Example `--cache **/*.png max-age=3600`.
- `--asset-cache` sets the default `Cache-Control` header for *assets*.
- `--entry-cache` sets the default `Cache-Control` header for *entries*.
- `--redirect-cache` sets the default `Cache-Control` header for directory index redirects.

#### Index Style

When statit encounters an `index.html` file it deploys it based on the `--index-style` parameter. By default, this parameter will deploy a file `foo/index.html` as `foo/` and provide a redirect from `foo` to `foo/`.

##### S3 Redirects

statit uses [S3's `x-amz-website-redirect-location` metadata option](https://docs.aws.amazon.com/AmazonS3/latest/userguide/how-to-page-redirect.html#redirect-requests-object-metadata) to create redirects. This option only works when using the [website endpoint](https://docs.aws.amazon.com/AmazonS3/latest/userguide/WebsiteEndpoints.html).

If you are using CloudFront and authenticated origin access you can't use the *website endpoint*. However, you can work around this with a CloudFront Functions Viewer response script.

```javascript
var REDIRECT_HEADER = "x-amz-website-redirect-location";

function handler(event) {
	var response = event.response;
	var headers = response.headers;

	if (response.statusCode != 200) return response;

	var locations = headers[REDIRECT_HEADER];
	if (!locations) return response;

	response.statusCode = 301;
	response.statusDescription = "Moved Permanently";

	delete headers[REDIRECT_HEADER];
	headers["location"] = { value: locations.value };

	return response;
}
```

#### Bucket Prefix

Don't deploy to the root of the bucket but a prefix. For example `--prefix=docs/` to deploy to the `docs/` subdirectory.

## Operation

### Usage

All statit state is maintained in the bucket itself. This is stored in the `.statit/` directory.

statit is not intended to be run concurrently on the same bucket. While statit attempts to provide reasonable behaviour in the face of concurrency this is not guaranteed. In particular splicing is likely, where half of the files are from one deployment and half are from another deployment.

Note: The state directory will typically be publicly visible in the website. However, all files are named with a timestamp and deployment ID which defaults to a random string with 47b of entropy. So as long as you don't allow bucket listing it is practically hidden. Furthermore, the only data stored in these manifests is a list of uploaded files. So if discovered they can list your website but nothing more.

If the state directory is removed then all *assets* will be considered new and will have their cleanup timer reset. statit will otherwise function normally.

## Installation

You can build from source with `cargo`. You will need a Rust toolchain installed.

```
cargo build --release
```

### Nix

A Nix build is available if you don't want to worry about managing dependencies. Simply clone the repo and run `nix-build` in the root or use the following command to download and install.

```sh
nix-env -if "https://gitlab.com/kevincox/statit/-/archive/master/statit-master.tar.gz"
```

#### Pre-built

You can also use the binaries built in CI. The latest version is always available, but older versions may be removed from the binary cache. If you want to ensure availability please mirror them yourself.

```sh
nix-env -i "$(curl https://kevincox.gitlab.io/statit/statit.nixpath)" \
	--extra-substituters https://kevincox.cachix.org \
	--extra-trusted-public-keys kevincox.cachix.org-1:OJCxM3dZ61ZpOY1nwCXQL33G2svrVBN7zsd2jswXtj0=
```

You can also just add the substitutes to the previous command, but that may not be a cache hit if CI is still building it or if you are using a different version of `<nixpkgs>`.
