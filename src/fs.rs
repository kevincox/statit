use futures::StreamExt;
use futures::TryStreamExt;

#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,serde::Deserialize,serde::Serialize)]
pub struct File {
	pub path: String,
	pub sha256: [u8; 32],
}

impl File {
	pub fn sha256_b64(&self) -> String {
		base64::Engine::encode(&base64::engine::general_purpose::STANDARD, &self.sha256)
	}
}

pub fn read_dir(
	dir: &str,
) -> impl futures::Stream<Item=anyhow::Result<File>> {
	let dir_len = dir.trim_end_matches('/').len() + 1;

	futures::stream::iter(
		walkdir::WalkDir::new(dir))
		.try_filter(|e| futures::future::ready(e.file_type().is_file()))
		.map(move |r| async move {
			let entry = match r {
				Ok(e) => e,
				Err(e) => return Err(e.into()),
			};

			tokio::task::spawn_blocking(move || {
				let path = entry.into_path();
				let mut file = std::fs::File::open(&path)?;
				let mut path = path.into_os_string()
					.into_string()
					.map_err(|path| anyhow::Error::msg(format!(
						"Path {:?} contains invalid Unicode.", path)))?;

				let mut hasher = sha2::Sha256::default();
				let mut buf = [0; 16 * 1024];
				loop {
					let size = std::io::Read::read(&mut file, &mut buf[..])?;
					if size == 0 {
						break
					}
					sha2::Digest::update(&mut hasher, &buf[..size]);
				}
				let mut sha256 = [0; 32];
				sha256.copy_from_slice(
					sha2::Digest::finalize(hasher).as_slice());

				path.replace_range(..dir_len, "");

				Ok(File{
					path,
					sha256,
				})
			}).await?
		})
		.buffer_unordered(8)
}
