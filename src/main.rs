use futures::StreamExt;
use futures::TryStreamExt;

mod args;
mod fs;
mod manager; use manager::*;
mod manifest; use manifest::*;

const INDEX_HTML: &str = "/index.html";

fn is_default(v: &(impl Default + PartialEq)) -> bool {
	v == &Default::default()
}

fn report_upload(path: &str, result: UploadResult) {
	match result {
		UploadResult::Created => eprintln!("+ {}", path),
		UploadResult::Unchanged => eprintln!("= {}", path),
		UploadResult::Updated{
			cache_control,
			content,
			content_type,
			redirect,
		} => {
			eprint!("~ {} (", path);
			let mut join = "";
			if cache_control {
				eprint!("{}cache", join);
				join = " ";
			}
			if content_type {
				eprint!("{}type", join);
				join = " ";
			}
			if redirect {
				eprint!("{}redirect", join);
			}
			if content {
				eprint!("{}content", join);
			}
			eprintln!(")");
		}
	}
}

fn expand_paths<'a>(f: &'a fs::File, index_style: &'a [args::IndexStyle]) -> impl Iterator<Item=&'a str> {
	let is_index = f.path.ends_with(INDEX_HTML);

	let non_index = (!is_index).then(|| f.path.as_str());

	let index = is_index
		.then(|| {
			assert!(!index_style.is_empty(), "--index-style must have at least one element.");
			index_style.iter().map(|style| match style {
				args::IndexStyle::IndexHtml => {
					f.path.as_str()
				}
				args::IndexStyle::NoSlash => {
					f.path.strip_suffix(INDEX_HTML).unwrap()
				}
				args::IndexStyle::TrailingSlash => {
					f.path.strip_suffix("index.html").unwrap()
				}
			})
		})
		.into_iter()
		.flatten();

	index.chain(non_index)
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
	let args: args::Args = clap::Parser::parse();

	let mut loader = aws_config::from_env();
	if let Some(endpoint) = args.endpoint {
		loader = loader.endpoint_url(endpoint);
	}
	let aws_config = loader.load().await;
	let s3 = aws_sdk_s3::Client::new(&aws_config);

	match args.command {
		args::Command::Deploy{
			asset_cache,
			entry_cache,
			redirect,
			redirect_cache,
			assets,
			cache,
			content_type,
			dry_run,
			id,
			index_style,
			mut directory,
		} => {
			let mut prefix = args.prefix;
			if !prefix.is_empty() && !prefix.ends_with('/') {
				prefix += "/";
			}

			let asset_globs = assets.into_iter()
				.map(|p| glob::Pattern::new(&p))
				.collect::<Result<Vec<_>, _>>()?;

			let mut cache_controls = Vec::with_capacity(cache.len()/2);
			let mut cache = cache.into_iter();
			while let Some(pattern) = cache.next() {
				cache_controls.push((
					glob::Pattern::new(&pattern)?,
					cache.next().unwrap()));
			}

			let mut content_types = Vec::with_capacity(content_type.len()/2);
			let mut content_type = content_type.into_iter();
			while let Some(pattern) = content_type.next() {
				content_types.push((
					glob::Pattern::new(&pattern)?,
					content_type.next().unwrap()));
			}

			if !directory.ends_with('/') {
				directory += "/";
			}
			let directory = &directory;

			let index_style = &index_style;

			let now = chrono::Utc::now();

			let mut manager = Manager::new(
				now,
				asset_globs,
				asset_cache,
				entry_cache,
				redirect_cache,
				cache_controls,
				content_types,
				s3,
				args.bucket.clone(),
				prefix,
				dry_run).await?;

			eprintln!("Listed current state.");

			let mut files: Vec<_> = fs::read_dir(&directory)
				.try_collect().await?;
			files.sort_unstable();

			eprintln!("Read input files.");

			let managerref = &manager;

			let mut assets = futures::stream::iter(
				files.iter()
				.filter(|f| manager.is_asset(&f.path))
				.map(|f| async move {
					let r = managerref.upload(
						f,
						&f.path,
						&format!("{}{}", directory, f.path),
						Stability::Asset).await?;
					Result::<_, anyhow::Error>::Ok((&f.path, r))
				}))
				.buffered(16);
			while let Some((path, r)) = assets.try_next().await? {
				report_upload(&path, r);
			}
			std::mem::drop(assets);

			eprintln!("Asset upload complete.");

			let mut entries = futures::stream::iter(
				files.iter()
				.filter(|f| !manager.is_asset(&f.path))
				.map(|f| async move {
					let mut paths = expand_paths(&f, index_style);
					let main_path = paths.next().unwrap();

					let r = managerref.upload(
						f,
						&main_path,
						&format!("{}{}", directory, f.path),
						Stability::Entry).await?;

					for path in paths {
						managerref.redirect(path, &main_path, Stability::Redirect).await?;
					}

					anyhow::Ok((&f.path, r))
				}))
				.buffered(16);
			while let Some((path, r)) = entries.try_next().await? {
				report_upload(&path, r);
			}
			std::mem::drop(entries);

			eprintln!("Entry upload complete.");

			let mut redirect_pairs = redirect.chunks_exact(2);
			let mut redirects = futures::stream::iter((&mut redirect_pairs)
				.map(|pair| async move {
					debug_assert_eq!(pair.len(), 2);
					let src = &pair[0];
					let src = src.strip_prefix('/').unwrap_or(src);
					let dst = &pair[1];
					let dst = dst.strip_prefix('/').unwrap_or(dst);

					let r = managerref.redirect(src, dst, Stability::Redirect).await?;
					anyhow::Ok((src, r))
				}))
				.buffered(32);
			while let Some((path, r)) = redirects.try_next().await? {
				report_upload(&path, r);
			}
			std::mem::drop(redirects);
			assert!(
				redirect_pairs.remainder().is_empty(),
				"Unpaired arguments {:?}", redirect_pairs.remainder());

			if !redirect.is_empty() {
				eprintln!("Redirect upload complete.");
			}

			manager.write_manifest(id, &files).await?;

			eprintln!("Wrote deploy manifest.");

			let mut deleted = manager.clean();
			while let Some(d) = deleted.next().await {
				match d {
					Ok(path) => eprintln!("- {}", path),
					Err(e) => eprintln!("Error deleting: {}", e),
				}
			}

			eprintln!("Cleanup complete.");
			eprintln!("All done.");
		}
	}

	Ok(())
}
