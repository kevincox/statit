#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,serde::Deserialize,serde::Serialize)]
pub struct ManifestFile {
	pub path: String,

	#[deprecated="Was always unused, still required to be present in old versions."]
	#[serde(default)]
	pub sha256: [u8; 32],

	/// If this file was not part of a new version, just known about.
	#[serde(default)]
	#[serde(skip_serializing_if="crate::is_default")]
	pub existing: bool,
}

#[derive(Debug,serde::Deserialize,serde::Serialize)]
pub struct Manifest {
	pub timestamp: chrono::DateTime<chrono::Utc>,
	pub files: Vec<ManifestFile>,
}

pub async fn download_one(
	s3: &aws_sdk_s3::Client,
	bucket: &str,
	path: &str,
) -> anyhow::Result<Manifest> {
	let download = s3.get_object()
		.bucket(bucket)
		.key(path)
		.send().await?;
	let contents = download.body
		.collect().await?
		.into_bytes();
	Ok(serde_json::from_slice(&contents)?)
}
