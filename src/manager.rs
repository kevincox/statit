use futures::StreamExt;
use futures::TryStreamExt;

const STATIT_PREFIX: &str = ".statit/";
const MANIFEST_PREFIX: &str = ".statit/deploys/";

const EMPTY_SHA256_B64: &str = "47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=";

pub enum Stability {
	Asset,
	Entry,
	Redirect,
}

pub struct Manager {
	pub asset_globs: Vec<glob::Pattern>,
	bucket: String,
	cache_control_asset: String,
	cache_control_entry: String,
	cache_control_redirect: String,
	cache_controls: Vec<(glob::Pattern, String)>,
	content_types: Vec<(glob::Pattern, String)>,
	dry_run: bool,
	current_files: tokio::sync::Mutex<std::collections::BTreeSet<String>>,
	known_files: std::collections::HashSet<String>,
	manifests: Vec<crate::Manifest>,
	now: chrono::DateTime<chrono::Utc>,
	prefix: String,
	s3: aws_sdk_s3::Client,
}

#[derive(Debug,PartialEq)]
pub enum UploadResult {
	Created,
	Unchanged,
	Updated{
		cache_control: bool,
		content: bool,
		content_type: bool,
		redirect: bool,
	},
}

impl Manager {
	pub async fn new(
		now: chrono::DateTime<chrono::Utc>,
		asset_globs: Vec<glob::Pattern>,
		cache_control_asset: String,
		cache_control_entry: String,
		cache_control_redirect: String,
		cache_controls: Vec<(glob::Pattern, String)>,
		content_types: Vec<(glob::Pattern, String)>,
		s3: aws_sdk_s3::Client,
		bucket: String,
		prefix: String,
		dry_run: bool,
	) -> anyhow::Result<Self> {
		let s3ref = &s3;
		let bucketref = &bucket;
		let prefixref = &prefix;

		enum Entry {
			File(String),
			Manifest(String, crate::Manifest),
		}

		let mut entries = s3.list_objects_v2()
			.bucket(&bucket)
			.prefix(&prefix)
			.into_paginator()
			.send()
			.map_ok(|response|
				futures::stream::iter(
					response.contents
						.into_iter()
						.flatten()
						.map(Result::<_, anyhow::Error>::Ok)))
			.try_flatten()
			.map(|file| async move {
				let file = file?;
				let mut path = file.key
					.ok_or_else(|| anyhow::Error::msg("Object has no key"))?;
				debug_assert!(path.starts_with(prefixref));
				path.replace_range(..prefixref.len(), "");
				
				if path.starts_with(MANIFEST_PREFIX) {
					let m = crate::download_one(s3ref, bucketref, &path).await?;
					Ok(Entry::Manifest(path, m))
				} else if path.starts_with(STATIT_PREFIX) {
					anyhow::bail!("Unexpected file {:?}", path);
				} else {
					Ok(Entry::File(path))
				}
			})
			.buffered(32);

		let recent_deploy_threshold = now - chrono::Duration::days(7);
		let mut manifests = Vec::<crate::Manifest>::new();
		let mut last_manifest_path = None::<String>;
		let mut current_files = std::collections::BTreeSet::new();
		let mut known_files = std::collections::HashSet::new();
		while let Some(r) = entries.try_next().await? {
			match r {
				Entry::File(path) => {
					current_files.insert(path);
				}
				Entry::Manifest(path, manifest) => {
					if manifest.timestamp > now {
						eprintln!("Manifest {:?} is in the future ({}), excluding from cleanup threshold calculation.", path, manifest.timestamp);
					} else if manifest.timestamp <= recent_deploy_threshold {
						debug_assert!(manifests.len() <= 1);
						if let Some(prior_manifest) = manifests.pop() {
							known_files.extend(prior_manifest.files.into_iter().map(|f| f.path));

							let prior_manifest_path = last_manifest_path.take().unwrap();
							known_files.insert(prior_manifest_path.clone());
							current_files.insert(prior_manifest_path);
						}
						manifests.clear();
					}

					manifests.push(manifest);
					last_manifest_path = Some(path);
				}
			}
		}
		std::mem::drop(entries);

		if let Some(manifest) = manifests.get(0) {
			// Discovered files *stopped* being live at the time of deploy so they can be cleaned up one deploy earlier.
			known_files.extend(
				manifest.files.iter()
					.filter(|f| f.existing)
					.map(|f| f.path.clone()));
		}

		Ok(Self {
			asset_globs,
			bucket,
			cache_control_asset,
			cache_control_entry,
			cache_control_redirect,
			cache_controls,
			content_types,
			dry_run,
			current_files: current_files.into(),
			known_files,
			manifests,
			now,
			prefix,
			s3,
		})
	}

	pub async fn write_manifest(
		&mut self,
		id: Option<String>,
		new_files: &[crate::fs::File],
	) -> anyhow::Result<()> {
		if self.dry_run {
			return Ok(())
		}

		let id = id.unwrap_or_else(||
			rand::distributions::DistString::sample_string(
				&rand::distributions::Alphanumeric,
				&mut rand::thread_rng(), 8));

		let manifest_path = format!(
			"{}{}{:?}_{}.json",
			self.prefix,
			MANIFEST_PREFIX,
			self.now,
			id);

		let size_estimate = self.known_files.len() + new_files.len();
		let mut seen = std::collections::HashSet::with_capacity(size_estimate);
		let mut files = Vec::with_capacity(size_estimate);

		for f in new_files {
			let new = seen.insert(&f.path);
			debug_assert!(new, "Duplicate file {:?}", f.path);

			#[allow(deprecated)]
			files.push(crate::ManifestFile{
				path: f.path.clone(),
				existing: false,

				sha256: f.sha256,
			});
		}

		for path in self.current_files.lock().await.iter() {
			if !seen.insert(&path) { continue }

			#[allow(deprecated)]
			files.push(crate::ManifestFile{
				path: path.clone(),
				existing: true,

				sha256: Default::default(),
			});
		}

		let manifest = crate::Manifest{
			timestamp: self.now,
			files,
		};

		self.s3.put_object()
			.bucket(&self.bucket)
			.key(manifest_path)
			.content_type("application/json")
			.body(serde_json::to_string(&manifest)?.into_bytes().into())
			.send().await?;

		Ok(())
	}

	pub fn is_asset(&self, path: &str) -> bool {
		self.asset_globs.iter().any(|p| p.matches(&path))
	}

	fn cache_control(&self, path: &str, stability: crate::Stability) -> &str {
		self.cache_controls.iter()
			.find_map(|(pattern, cache)|
				pattern.matches(&path).then(|| cache.as_str()))
			.unwrap_or_else(|| match stability {
				crate::Stability::Asset => &self.cache_control_asset,
				crate::Stability::Entry => &self.cache_control_entry,
				crate::Stability::Redirect => &self.cache_control_redirect,
			})
	}

	fn content_type(&self, file: &crate::fs::File) -> &str {
		self.content_types.iter()
			.find_map(|(pattern, mime)|
				pattern.matches(&file.path).then(|| mime.as_str()))
			.or_else(||
				mime_guess::from_path(&file.path)
					.first_raw())
			.unwrap_or("application/octet-stream")
	}

	async fn diff(
		&self,
		path: &str,
		key: &str,
		content_type: &str,
		cache: &str,
		sha256: &str,
		redirect: Option<&str>,
	) -> anyhow::Result<UploadResult> {
		if !self.current_files.lock().await.remove(path) {
			return Ok(UploadResult::Created);
		}

		let head = self.s3.head_object()
			.bucket(&self.bucket)
			.key(key)
			.checksum_mode(aws_sdk_s3::types::ChecksumMode::Enabled)
			.send().await?;

		let cache_control = head.cache_control()
			.filter(|c| c == &cache).is_none();
		let content = head.checksum_sha256()
			.filter(|c| c == &sha256).is_none();
		let content_type = head.content_type()
			.filter(|c| c == &content_type).is_none();
		let redirect = head.website_redirect_location() != redirect;

		if !(cache_control || content || content_type || redirect) {
			return Ok(UploadResult::Unchanged)
		}

		Ok(UploadResult::Updated{
			cache_control,
			content,
			content_type,
			redirect,
		})
	}

	pub async fn upload(
		&self,
		file: &crate::fs::File,
		path: &str,
		source: &str,
		stability: crate::Stability,
	) -> anyhow::Result<UploadResult> {
		let key = format!("{}{}", self.prefix, path);
		let content_type = self.content_type(file);
		let cache = self.cache_control(&file.path, stability);

		let local_sha256 = file.sha256_b64();

		let r = self.diff(path, &key, content_type, cache, &local_sha256, None).await?;
		if self.dry_run || matches!(r, UploadResult::Unchanged) {
			return Ok(r)
		}

		self.s3.put_object()
			.bucket(&self.bucket)
			.key(key)
			.content_type(content_type)
			.cache_control(cache)
			.checksum_sha256(local_sha256)
			.body(aws_sdk_s3::primitives::ByteStream::read_from()
				.path(source)
				.build().await?)
			.send().await?;

		Ok(r)
	}

	pub async fn redirect(
		&self,
		src: &str,
		dst: &str,
		stability: crate::Stability,
	) -> anyhow::Result<UploadResult> {
		let key_src = format!("{}{}", self.prefix, src);
		let cache = self.cache_control(&src, stability);
		let key_dst = format!("/{}{}", self.prefix, dst);

		let r = self.diff(
			src,
			&key_src,
			"application/octet-stream",
			cache,
			EMPTY_SHA256_B64,
			Some(&key_dst)).await?;
		if self.dry_run || r == UploadResult::Unchanged {
			return Ok(r)
		}

		self.s3.put_object()
			.bucket(&self.bucket)
			.key(key_src)
			.website_redirect_location(key_dst)
			.cache_control(cache)
			.checksum_sha256(EMPTY_SHA256_B64)
			.send().await?;

		Ok(r)
	}

	pub fn clean(self) -> impl futures::Stream<Item=anyhow::Result<String>> {
		let Self{
			asset_globs,
			bucket,
			dry_run,
			current_files,
			manifests,
			prefix,
			s3,
			..
		} = self;

		let mut current_files = current_files.into_inner();

		manifests.into_iter()
			.flat_map(|m| m.files)
			.filter(|f| !f.existing)
			.filter(|f| asset_globs.iter().any(|p| p.matches(&f.path)))
			.for_each(|f| {
				current_files.remove(&f.path);
			});

		current_files.retain(|f| self.known_files.contains(f));

		futures::stream::iter(current_files.into_iter())
			.chunks(1000)
			.map(move |files| {
				let p = s3.delete_objects()
					.bucket(&bucket)
					.delete(aws_sdk_s3::types::Delete::builder()
						.set_objects(Some(files.iter()
							.map(|f| aws_sdk_s3::types::ObjectIdentifier::builder()
								.key(format!("{}{}", prefix, f))
								.build())
							.collect()))
						.build())
					.send();
				async move {
					if dry_run {
						return Ok(files)
					}

					p.await?;
					Result::<_, anyhow::Error>::Ok(files)
				}
			})
			.buffered(4)
			.map_ok(|files| futures::stream::iter(files.into_iter().map(Ok)))
			.try_flatten()
	}
}
