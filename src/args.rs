#[derive(Clone,Debug,clap::ValueEnum)]
pub enum IndexStyle {
	/// Store the file as `directory/index.html`.
	IndexHtml,
	/// Store the file as `directory`.
	NoSlash,
	/// Store the file as `directory/`.
	TrailingSlash,
}

#[derive(Debug,clap::Subcommand)]
pub enum Command {
	Deploy {
		/// Cache-Control header to pass for assets.
		#[clap(long, default_value="max-age=31536000,immutable")]
		asset_cache: String,

		/// List of patterns for "asset" files.
		#[clap(long, short, default_value="a/**")]
		assets: Vec<String>,

		/// Set the Cache-Control header for matching files.
		///
		/// The first argument is the glob pattern for which files the Cache-Control applies to. The second argument is the Cache-Control to apply.
		///
		/// The first matching pattern will be used, files not matched by any of these rules will use the cache settings from the `--asset-cache` or `--entry-cache` setting as applicable.
		#[clap(long, value_names=["GLOB", "CACHE-CONTROL"])]
		cache: Vec<String>,

		/// Set the Content-Type header.
		///
		/// The first argument is the pattern for which files the Content-Type applies to. The second argument is the Content-Type to apply.
		///
		/// The first matching pattern will be used, files not matched by any of these rules will have a type guessed based on their extension. Note that the guessed extension may change with different releases of statit (even minor releases).
		#[clap(long, value_names=["GLOB", "CONTENT-TYPE"])]
		content_type: Vec<String>,

		directory: String,

		/// Don't actually write any files.
		///
		/// Calculate what would have been done and log it but don't modify anything. Note that the current state is still read in order to calculate what actions would have been done.
		#[clap(short='n', long, default_value_t=false)]
		dry_run: bool,

		/// Cache-Control header to pass for entries.
		///
		/// The default value is 10min with no revalidation, 1h with background revalidation and 30d on error.
		#[clap(long, default_value="max-age=600,stale-while-revalidate=3600,stale-if-error=2592000")]
		entry_cache: String,

		/// The ID to use for this deploy. By default a unique ID is generated.
		#[clap(long)]
		id: Option<String>,

		/// How to store index files.
		///
		/// Can be specified multiple times. Subsequent styles will redirect to the "primary" style.
		#[clap(long, value_enum, default_values_t=[IndexStyle::TrailingSlash, IndexStyle::NoSlash])]
		index_style: Vec<IndexStyle>,

		/// Add a redirect.
		///
		/// The first argument is the URL path to redirect. The second argument is the redirect target.
		///
		/// No globing or matching is performed.
		#[clap(long, value_names=["SOURCE", "TARGET"])]
		redirect: Vec<String>,

		/// Cache-Control header to pass for index redirects.
		///
		/// The default value is 24h with no revalidation, 48h with background revalidation and 30d on error.
		#[clap(long, default_value="max-age=86400,stale-while-revalidate=172800,stale-if-error=2592000")]
		redirect_cache: String,
	},
}

/// Deploy a static site.
#[derive(Debug,clap::Parser)]
#[clap(version)]
pub struct Args {
	#[clap(subcommand)]
	pub command: Command,

	/// The S3 endpoint to use. Defaults to AWS.
	#[clap(long)]
	pub endpoint: Option<String>,

	/// The bucket to upload to.
	#[clap(long)]
	pub bucket: String,

	/// Upload files under this prefix.
	#[clap(long, default_value_t)]
	pub prefix: String,
}
